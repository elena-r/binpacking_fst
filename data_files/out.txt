
----------------------------------------------------------------------------
Algorithm: Normal; Result status: OPTIMAL; Solving time: 00:00:00.31
----------------------------------------------------------------------------

Bin A00(not available) has capacity 6500 and contains 
	IT05 of length 4700
	IT08 of length 1500

Bin A01(not available) has capacity 6500 and contains 
	IT04 of length 4800
	IT07 of length 1470

Bin A02(not available) has capacity 6500 and contains 
	IT00 of length 1000
	IT01 of length 1000
	IT02 of length 1800
	IT06 of length 2600

Bin A03(not available) has capacity 6500 and contains 
	IT03 of length 2150
	IT09 of length 1500
	IT10 of length 2400


---------------------------------------------
	 BaseSolver numBins = 4
---------------------------------------------


----------------------------------------------------------------------------
Algorithm: Priority; Result status: OPTIMAL; Solving time: 00:00:00.18
----------------------------------------------------------------------------

Bin A00(not available) has capacity 6500 and contains 
	IT05 of length 4700
	IT07 of length 1470

Bin A01(not available) has capacity 6500 and contains 
	IT03 of length 2150
	IT08 of length 1500
	IT10 of length 2400

Bin A02(not available) has capacity 6500 and contains 
	IT04 of length 4800
	IT09 of length 1500

Bin A03(not available) has capacity 6500 and contains 
	IT00 of length 1000
	IT01 of length 1000
	IT02 of length 1800
	IT06 of length 2600


---------------------------------------------
	 BaseSolver numBins = 4
	 PrioritySolver numBins = 4
---------------------------------------------

