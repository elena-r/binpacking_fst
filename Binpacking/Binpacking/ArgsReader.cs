﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Binpacking
{
    /// <summary>
    /// This class manages the input arguments for the program
    /// </summary>
    public class ArgsReader
    {
        private static bool isOk = true;

        public static bool HandleArgs(List<string> args)
        {
            if (args.Count == 1 & args[0] == "--help")
                DisplayHelp();
            else if(args.Count > 1)
                SaveParams(args);
            else
            {
                Console.WriteLine("Type --help to se list of available commands");
                isOk = false;
            }

            return isOk;
            
        }
        private static void DisplayHelp()
        {
            Console.WriteLine("\nAvailable commands:\n\n" +
                "<binspath> <itemspath> [-O=<outputpath>] [-M=<mode>] [-S=<solver>]\t- create bin and item instances based on the input text files and execute binpacking on them \n" +
                "\t\t <outputpath> if specified, save the results in that file instead of showing them on stdout \n" +
                "\t\t <mode> can be either \"normal\", \"priority\" or not be specified, in which case it display the best result out of the two \n"+
                "\t\t <solver> if specified, use this solver instead of the default one (CBC); possible values are CBC, BOP, SAT \n\n" +
                "--help\t- display this guide\n");
            isOk = false;
        }

        private static void SaveParams(List<string> args)
        {
            if (!File.Exists(args[0]))
            {
                Console.WriteLine("[ERROR_ArgsReader] The bins file at " + args[0] + " does not exist");
                isOk = false;
                return;
            }

            if (!File.Exists(args[1]))
            {
                Console.WriteLine("[ERROR_ArgsReader] The items file at " + args[1] + " does not exist");
                isOk = false;
                return;
            }

            Settings.BinPath = args[0];
            Settings.ItemPath = args[1];

            for(int i = 2; i < args.Count; i++)
            {
                ParseOpt(args[i]);
            }
        }

        private static void ParseOpt(string str)
        {
            string arg = str.Substring(3);
            if (str.Contains("-O="))
                Settings.OutputPath = arg;
            else if (str.Contains("-M="))
                Settings.Mode = CheckMode(arg);
            else if (str.Contains("-S="))
                Settings.Solver = CheckSolver(arg);
            else
            {
                Console.WriteLine("[ERROR_ArgsReader] Option " + str + " not recognized");
                isOk = false;
            }
        }

        private static string CheckMode(string mode)
        {
            if (mode != "normal" && mode != "priority")
            {
                Console.WriteLine("[ERROR_ArgsReader] Incorrect value for -M option \n allowed values are \"normal\" and \"priority\" (without \"\")");
                isOk = false;
                return "";
            }
            return mode;
        }

        private static string CheckSolver(string solver)
        {
            if (solver != "CBC" && solver != "SAT" && solver != "BOP")
            {
                Console.WriteLine("[ERROR_ArgsReader] Incorrect value for -S option \n allowed values are \"CBC\", \"SAT\" and \"BOP\" (without \"\")");
                isOk = false;
                return "";
            }
            Settings.SolversDictionary.TryGetValue(solver, out string sName);
            return sName;
        }
        
    }
}
