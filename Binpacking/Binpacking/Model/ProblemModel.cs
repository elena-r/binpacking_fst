﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Binpacking.Model
{
    public class ProblemModel : IProblemModel, IProblemParams
    {
        // Bins and Items
        /// <summary>
        /// Array that contains the lengths of the items in order
        /// </summary>
        public int[] Lenghts { get; private set; }

        /// <summary>
        /// Array that contains the capacities of the bins in order
        /// </summary>
        public int[] Capacities { get; private set; }

        public int NumItems { get; private set; }

        public int NumBins { get; private set; }

        public List<Bin> Bins { get; private set; }

        public List<Item> Items { get; private set; }


        // Problem constants
        public int BladeWidth { get; set; } = 5;

        public int BinMargin { get; set; } = 1;


        private Dictionary<int, string> binMap = new Dictionary<int, string>();
        private Dictionary<int, string> itemMap = new Dictionary<int, string>();


        public string ParseData(Bin[] bins, Item[] items)
        {
            this.NumBins = bins.Length;
            this.NumItems = items.Length;

            this.Bins = new List<Bin>(bins);
            this.Items = new List<Item>(items);

            this.Capacities = new int[NumBins];
            for(int i = 0; i < NumBins; i++)
            {
                if(BinMargin >= bins[i].Capacity)
                    return "[ERROR_ProblemModel] The BinMargin parameter is greater than at least one bin capacity";
                this.Capacities[i] = bins[i].Capacity - BinMargin;
            }
            SetBinMap(bins);

            this.Lenghts = new int[NumItems];
            bool inclinedDX = false;
            for (int i = 0; i < NumItems; i++)
            {
                int margin = BladeWidth;
                if (items[i].AngleSX == 45 || inclinedDX)
                {
                    margin = Convert.ToInt32(Math.Round(BladeWidth * Math.Sqrt(2)));
                    inclinedDX = false;
                }
                if (items[i].AngleDX == 45)
                {
                    inclinedDX = true;
                }

                this.Lenghts[i] = items[i].Length + margin;
            }
            SetItemMap(items);

            string err;
            if ((err = DoCheck(this.Capacities, this.Lenghts)) != null)
                return err;

            return null;
        }

        /// <summary>
        /// Perform validity checks on input arrays (bins' capacities and
        /// items' lengths) and return string with error if there is one.
        /// Checks should be performed only for the complete binpacking problem.
        /// </summary>
        /// <returns> Returns null if nothing's wrong </returns>
        public string DoCheck(int[] capacities, int[] lengths)
        {
            string err = CheckInputs(capacities, lengths);
            if (err != null)
            {
                return err;
            }
            return null;
        }


        public string GetBinStrId(int numId)
        {
            if (!binMap.TryGetValue(numId, out string res))
            {
                Console.WriteLine("Error: no match was found for bin's id");
                return "[ERROR_ProblemModel]";
            }
            return res;
        }

        public string GetItemStrId(int numId)
        {
            if (!itemMap.TryGetValue(numId, out string res))
            {
                Console.WriteLine("Error: no match was found for item's id");
                return "[ERROR_ProblemModel]";
            }
            return res;
        }


        private void SetBinMap(Bin[] bins)
        {
            for(int i = 0; i < NumBins; i++)
            {
                binMap.Add(i, bins[i].Id);
            }
        }

        private void SetItemMap(Item[] items)
        {
            for (int i = 0; i < NumItems; i++)
            {
                itemMap.Add(i, items[i].Id);
            }
        }

        private string CheckInputs(int[] capacities, int[] lenghts)
        {
            List<int> binsList = new List<int>(capacities);
            List<int> itemsList = new List<int>(lenghts);

            /* checks that there's no item bigger than the biggest bin */
            int maxCapacity = binsList.OrderByDescending(bin => bin).First();
            int maxLength = itemsList.OrderByDescending(item => item).First();
            if (maxLength > maxCapacity)
            {
                return "[ERROR_ProblemModel] There is at least one item that doesn't fit in any bin";
            }

            /* checks that the sum of item lengths can fit in the sum of bins */
            int sumC = 0;
            int sumL = 0;
            binsList.ForEach(bin => sumC += bin);
            itemsList.ForEach(item => sumL += item);
            if (sumL > sumC)
            {
                return "[ERROR_ProblemModel] The sum of item lenghts surpasses the sum of bins capacities";
            } 

            return null;
        }

    }
}
