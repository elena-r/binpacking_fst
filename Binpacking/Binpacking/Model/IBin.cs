﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Binpacking.Model
{
    /// <summary>
    /// Model a bin in which to pack the items; in this case it
    /// represents a metal bar to be cut.
    /// </summary>
    public interface IBin
    {
        string Id { get; }

        int Capacity { get; }

        /// <summary>
        /// Indicates whether this bin readily available, thus prioritizing its use
        /// </summary>
        bool IsAvailable { get; }
    }
}
