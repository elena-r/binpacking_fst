﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Binpacking.Model
{
    /// <summary>
    /// Models an item to packed to be packed, in this case represents case a metal bar.
    /// </summary>
    public interface IItem
    {
        string Id { get; }
        int Length { get; }

        /// <summary>
        /// The angle with which to cut the item on the left side;
        /// At the moment recognized inputs are 90 and 45
        /// </summary>
        int AngleSX { get; }

        /// <summary>
        /// The angle with which to cut the item on the right side;
        /// At the moment recognized inputs are 90 and 45
        /// </summary>
        int AngleDX { get; }
    }
}
