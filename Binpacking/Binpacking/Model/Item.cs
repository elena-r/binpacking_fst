﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Binpacking.Model
{
    public class Item : IItem
    {
        public string Id { get; private set; }

        public int Length { get; private set; }

        public int AngleSX { get; private set; } = 90;

        public int AngleDX { get; private set; } = 90;

        public Item(string id, int length)
        {
            this.Id = id;
            this.Length = length;
        }

        public Item(string id, int length, int angleSX, int angleDX) : this(id, length)
        {
            if(angleSX != 90 && angleSX != 45 || angleDX != 90 && angleDX != 45)
            {
                throw new Exception("Input of non-supported item angle");
            }
            this.AngleSX = angleSX;
            this.AngleDX = angleDX;
        }
    }
}
