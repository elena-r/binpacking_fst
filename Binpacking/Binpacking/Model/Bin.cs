﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Binpacking.Model
{
    public class Bin : IBin
    {
        public string Id { get; private set; }

        public int Capacity { get; private set; }

        public bool IsAvailable { get; private set; }


        public Bin(string id, int capacity, bool isAvailable)
        {
            this.Id = id;
            this.Capacity = capacity;
            this.IsAvailable = isAvailable;
        }
    }
}
