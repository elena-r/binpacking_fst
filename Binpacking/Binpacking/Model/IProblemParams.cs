﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Binpacking.Model
{
    /// <summary>
    /// In this class constant values for the problem can be set
    /// </summary>
    public interface IProblemParams
    {
        /// <summary>
        /// The width of the blade that cuts the items from the bin;
        /// when set, it adds this margin to the left of all input items.
        /// </summary>
        int BladeWidth { get; }

        /// <summary>
        /// Margin of the bins that can't be used (sum of left+right margin).
        /// It is subtracted from all bin capacities.
        /// </summary>
        int BinMargin { get; }
    }
}
