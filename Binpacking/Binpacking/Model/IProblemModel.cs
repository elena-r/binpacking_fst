﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Binpacking.Model
{
    public interface IProblemModel
    {
        /// <summary>
        /// Array that contains the lengths of the items in order
        /// </summary>
        int[] Lenghts { get; }

        /// <summary>
        /// Array that contains the capacities of the bins in order
        /// </summary>
        int[] Capacities { get; }

        int NumItems { get; }

        int NumBins { get; }

        List<Bin> Bins { get; }

        List<Item> Items { get;}


        /// <summary>
        /// Accepts the problem models and parses in the correct
        /// format for the linear solver.
        /// </summary>
        /// <param name="bins">The bins in which to pack the items</param>
        /// <param name="item">The items to be packed</param>
        /// <returns> If there's some inconsistency with input data, this
        /// method returns an error message </returns>
        public string ParseData(Bin[] bins, Item[] item);

        /// <summary>
        /// Returns the bin's string id instead of the array id.
        /// </summary>
        /// <param name="numId">The array id that identifies the bin</param>
        /// <returns>Returns the bin's string id corresponding to the array id.</returns>
        public string GetBinStrId(int numId);

        /// <summary>
        /// Returns the items's string id instead of the array id.
        /// </summary>
        /// <param name="numId">The array id that identifies the item</param>
        /// <returns>Returns the item's string id corresponding to the array id.</returns>
        public string GetItemStrId(int numId);
    }
}
