﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Binpacking.Model
{
    public interface ISolutionModel
    {
        Dictionary<Bin, List<Item>> Solution { get; }
        List<Item> Items { get; }
        string ResultStatus { get; set; }
        string ElapsedTime { get; set; }

        /// <summary>
        /// Indicates what kind of algorithm this solution was calculated with
        /// (for example "Priority" or "Normal"); will be used in PrintResults
        /// </summary>
        string Type { get; set; }

        /// <summary>
        /// Save the result of the binpacking solver
        /// </summary>
        void AddItemToBin(Bin bin, Item item);

        /// <summary>
        /// Print the results on the stdout 
        /// </summary>
        void PrintResults();

        /// <summary>
        /// Print the results in the specified output file; if it does not
        /// exist it is created, otherwise the results are appended
        /// </summary>
        /// <param name="outFile">The path to the output file</param>
        void PrintResults(string outFile);

    }
}
