﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Binpacking.Model
{
    public class SolutionModel : ISolutionModel
    {
        public Dictionary<Bin, List<Item>> Solution { get; private set; } = new Dictionary<Bin, List<Item>>();
        public List<Item> Items { get; private set; } = new List<Item>(); // used to remove packed items from next iteration of packing
        public string ResultStatus { get ; set ; }
        public string ElapsedTime { get; set; }

        /// <summary>
        /// Indicates what kind of algorithm this solution was calculated with
        /// (for example "Priority" or "Normal"); will be used in PrintResults
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Save the result of the binpacking solver
        /// </summary>
        public void AddItemToBin(Bin bin, Item item)
        {
            if (Solution.TryGetValue(bin, out List<Item> itemList))
            {
                itemList.Add(item);
            }
            else
            {
                Solution.Add(bin, new List<Item>() { item });
            }
            Items.Add(item);
        }

        /// <summary>
        /// Print the results on the stdout 
        /// </summary>
        public void PrintResults()
        {
            Console.WriteLine("\n----------------------------------------------------------------------------\n" +
                "Algorithm: " + Type + "; Result status: " + ResultStatus + "; Solving time: " + ElapsedTime +
                "\n----------------------------------------------------------------------------\n");
            
            foreach(Bin bin in Solution.Keys)
            {
                Console.WriteLine("Bin " + bin.Id + "(" + ConvertIsAvaiable(bin.IsAvailable) + ")" + " has capacity " + bin.Capacity + " and contains ");
                Solution.GetValueOrDefault(bin).ForEach(item => Console.WriteLine("\t" + item.Id + " of length " + item.Length));
                Console.Write("\n");
            }
        }

        /// <summary>
        /// Print the results in the specified output file; if it does not
        /// exist it is created, otherwise the results are appended
        /// </summary>
        /// <param name="outFile">The path to the output file</param>
        public void PrintResults(string outFile)
        {
            if (!File.Exists(outFile))
            {
                using (StreamWriter sw = File.CreateText(outFile))
                {
                    sw.WriteLine("\n----------------------------------------------------------------------------\n" +
                                "Algorithm: " + Type + "; Result status: " + ResultStatus + "; Solving time: " + ElapsedTime +
                                "\n----------------------------------------------------------------------------\n");

                    foreach (Bin bin in Solution.Keys)
                    {
                        sw.WriteLine("Bin " + bin.Id + "(" + ConvertIsAvaiable(bin.IsAvailable) + ")" + " has capacity " + bin.Capacity + " and contains ");
                        Solution.GetValueOrDefault(bin).ForEach(item => sw.WriteLine("\t" + item.Id + " of length " + item.Length));
                        sw.Write("\n");
                    }

                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(outFile))
                {
                    sw.WriteLine("\n----------------------------------------------------------------------------\n" +
                    "Algorithm: " + Type + "; Result status: " + ResultStatus + "; Solving time: " + ElapsedTime +
                    "\n----------------------------------------------------------------------------\n");

                    foreach (Bin bin in Solution.Keys)
                    {
                        sw.WriteLine("Bin " + bin.Id + "(" + ConvertIsAvaiable(bin.IsAvailable) + ")" + " has capacity " + bin.Capacity + " and contains ");
                        Solution.GetValueOrDefault(bin).ForEach(item => sw.WriteLine("\t" + item.Id + " of length " + item.Length));
                        sw.Write("\n");
                    }
                }
            }

            
        }

        private string ConvertIsAvaiable(bool b)
        {
            return (b == true) ? "available" : "not available";
        }
    }
}
