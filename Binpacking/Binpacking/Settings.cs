﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Binpacking
{
    /// <summary>
    /// This class contains user settings
    /// </summary>
    public static class Settings
    {
        public static string BinPath { get; set; } = "";
        public static string ItemPath { get; set; } = "";
        public static string OutputPath { get; set; } = "";
        public static string Mode { get; set; } = "";
        public static string Solver { get; set; } = "CBC_MIXED_INTEGER_PROGRAMMING";

        public static Dictionary<string, string> SolversDictionary { get;  } 
            = new Dictionary<string, string>() { {"CBC", "CBC_MIXED_INTEGER_PROGRAMMING"},
                                                { "SAT", "SAT_INTEGER_PROGRAMMING" },
                                                { "BOP", "BOP_INTEGER_PROGRAMMING"}, //BINARY values; is faster and more accurate, however the solver parameters need to be correctly set otherwise it won't work properly
                                                { "SCIP", "SCIP_MIXED_INTEGER_PROGRAMMING"} }; // error when creating solver


    }
}
