﻿using Binpacking.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Binpacking.DataReader
{
    public class SimpleReader : IDataReader
    {
        /// <summary>
        /// Parses data from files to create data structures to be fed to
        /// the binpacking algorithm
        /// </summary>
        /// <param name="binsPath">The path to the text file with data to
        /// create bins</param>
        /// <param name="itemsPath">The path to the text file with data to
        /// create items</param>
        /// <returns>A data structure that can be used by the solver</returns>
        public IProblemModel ReadFromFile(string binsPath, string itemsPath)
        {
            if (!CheckFile(binsPath) || !CheckFile(itemsPath))
            {
                return null;
            }

            /*  ---  read bins data  --- */
            List<Bin> bins = new List<Bin>();
            using (StreamReader sr = File.OpenText(binsPath))
            {
                string line;
                
                while((line = sr.ReadLine()) != null)
                {
                    line.Trim();
                    string[] words = line.Split(new char[] { ' ', '\n' });
                    bins.Add(new Bin(words[0], Convert.ToInt32(words[1]), Convert.ToBoolean(words[2])));
                }
            }

            /*  --- read items data --- */
            List<Item> items = new List<Item>();
            using(StreamReader sr = File.OpenText(itemsPath))
            {
                string line;

                while((line = sr.ReadLine()) != null)
                {
                    line.Trim();
                    string[] words = line.Split(new char[] { ' ', '\n' });
                    if (words.Length == 2)
                    {
                        items.Add(new Item(words[0], Convert.ToInt32(words[1])));
                    } else if (words.Length == 4)
                    {
                        items.Add(new Item(words[0], Convert.ToInt32(words[1]), Convert.ToInt32(words[2]), Convert.ToInt32(words[3])));
                    } else
                    {
                        Console.WriteLine("[ERROR_SimpleReader] item text file has incorrect format");
                        Console.WriteLine("\t\t text file @" + itemsPath);
                        return null;
                    }
                }
            }

            /*  --- prepare data model  --- */
            IProblemModel pm = new ProblemModel();
            string err;
            if ((err = pm.ParseData(bins.ToArray(), items.ToArray())) != null)
            {
                Console.WriteLine(err);
                return null;
            }
            return pm;
        }


        private bool CheckFile(string path)
        {
            if (!File.Exists(path))
            {
                Console.WriteLine("[ERROR_SimpleReader] File " + path + " doesn't exist");
                return false;
            }
            return true;
        }

    }
}
