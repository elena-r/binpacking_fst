﻿using System;
using System.Collections.Generic;
using System.Text;
using Binpacking.Model;

namespace Binpacking.DataReader
{
    public interface IDataReader
    {
        /// <summary>
        /// Parses data from files to create data structures to be fed to
        /// the binpacking algorithm
        /// </summary>
        /// <param name="binsPath">The path to the text file with data to
        /// create bins</param>
        /// <param name="itemsPath">The path to the text file with data to
        /// create items</param>
        /// <returns>A data structure that can be used by the solver</returns>
        IProblemModel ReadFromFile(string binsPath, string itemsPath);
    }
}
