﻿using System;
using System.Diagnostics;
using System.Linq;
using Google.OrTools.LinearSolver;
using Binpacking.Model;

/* Monodimensional Bin Packing using Google's OR-Tools library
 * Implemented following the template provided at
 * https://developers.google.com/optimization/bin/bin_packing
 */

namespace Binpacking.LinearSolver
{
    public static class BaseSolver
    {
        public static bool OutputEnabled { get; set; } = false;
        public static double MipGap { get; set; } = -1;
        public static int TimeLimitMs { get; set; } = -1;
        private static string SolverName { get; set; } = "CBC_MIXED_INTEGER_PROGRAMMING";


        /// <summary>
        /// Execute the binpacking algorithm on the input data and save results
        /// in the solution. Additionally, specify which solver to use (CBC, BOP, SAT)
        /// </summary>
        /// <param name="data">Reference to an already existing instance of IProblemModel</param>
        /// <param name="solution">Reference to an already existing instance of ISolutionModel</param>
        /// <param name="solverName">Name of the solver to be used</param>
        public static void Pack(IProblemModel data, ISolutionModel solution, string solverName)
        {
            SolverName = solverName;
            Pack(data, solution);
        }

        /// <summary>
        /// Execute the binpacking algorithm on the input data and save results
        /// in the solution.
        /// </summary>
        /// <param name="data">Reference to an already existing instance of IProblemModel</param>
        /// <param name="solution">Reference to an already existing instance of ISolutionModel</param>
        public static void Pack(IProblemModel data, ISolutionModel solution)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            //Create the linear solver-----------------------------------------------------------------
            Solver solver = Solver.CreateSolver("SimpleMipProgram", SolverName);

            /* ~ SETTINGS ~ */
            if(OutputEnabled) solver.EnableOutput();
            MPSolverParameters param = new MPSolverParameters();
            if (MipGap >= 0)
            {
                param.SetDoubleParam(MPSolverParameters.DoubleParam.RELATIVE_MIP_GAP, MipGap);
            }

            if (TimeLimitMs >= 0)
            {
                if(SolverName == "BOP_INTEGER_PROGRAMMING")
                    solver.SetSolverSpecificParametersAsString("max_time_in_seconds:" + TimeLimitMs*1000);
                else
                    solver.SetTimeLimit(TimeLimitMs);
            }

            //Create the variables---------------------------------------------------------------------

            //variables x[(i, j)] whose value is 1 if item i is placed in bin j, 0 otherwise
            Variable[,] x = new Variable[data.NumItems, data.NumBins];
            for (int i = 0; i < data.NumItems; i++)
            {
                for (int j = 0; j < data.NumBins; j++)
                {
                    x[i, j] = solver.MakeIntVar(0, 1, String.Format("x_{0}_{1}", i, j));
                }
            }

            //variables y[j] whose value is 1 if bin j is used, 0 otherwise
            Variable[] y = new Variable[data.NumBins];
            for (int j = 0; j < data.NumBins; j++)
            {
                y[j] = solver.MakeIntVar(0, 1, String.Format("y_{0}", j));
            }

            //Define the constraints-------------------------------------------------------------------

            /* Each item must be placed in exactly one bin.
               This constraint is set by requiring that the sum of x[i, j]
               over all bins j is equal to 1 */
            for (int i = 0; i < data.NumItems; ++i)
            {
                LinearExpr sum = new LinearExpr();
                for (int j = 0; j < data.NumBins; ++j)
                {
                    sum += x[i, j];
                }
                solver.Add(sum == 1);
            }
            
            /* The total weight packed in each bin can't exceed its capacity */
            for (int j = 0; j < data.NumBins; ++j)
            {
                LinearExpr weight = new LinearExpr();
                for (int i = 0; i < data.NumItems; ++i)
                {
                    weight += data.Lenghts[i] * x[i, j];
                }
                /* the multiplication by y[j] forces y[j] to equal 1 if any
                   item is packed in bin j. */
                try
                {
                    solver.Add(weight <= y[j] * data.Capacities[j]);
                }
                catch (IndexOutOfRangeException e)
                {
                    Console.WriteLine("[" + e.Message + "] Please initialize binCapacity with at least numItems values");
                }
            }


            // Define the objective--------------------------------------------------------------------

            Objective objective = solver.Objective();
            //LinearExpr numBinsUsed = null;
            for (int j = 0; j < data.NumBins; ++j)
            {
                //numBinsUsed += y[j];
                objective.SetCoefficient(y[j], 1);
            }
            objective.SetMinimization();


            // Call the solver-------------------------------------------------------------------------
            Solver.ResultStatus resultStatus = solver.Solve(param);
            solution.ResultStatus = resultStatus.ToString();



            stopwatch.Stop();
            // Get the elapsed time as a TimeSpan value.
            TimeSpan ts = stopwatch.Elapsed;

            // Check that the problem has an optimal solution
            /* if (resultStatus != Solver.ResultStatus.OPTIMAL)
             {
                 Console.WriteLine("The problem does not have an optimal solution!");
                 return;
             }*/
            
            
            for (int j = 0; j < data.NumBins; ++j)
            {
                if (y[j].SolutionValue() == 1)
                {
                    for (int i = 0; i < data.NumItems; ++i)
                    {
                        if (x[i, j].SolutionValue() == 1)
                        {
                            solution.AddItemToBin(data.Bins.Find(b => b.Id == data.GetBinStrId(j)), data.Items.Find(it => it.Id == data.GetItemStrId(i)));
                        }
                    }
                }
            }
            
            solution.ElapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10);
            solution.Type = "Normal";
        }
    }
}
