﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Binpacking.DataReader;
using Binpacking.Model;
using Binpacking.LinearSolver;
using System.Linq;

namespace Binpacking
{
    public static class Testing
    {
        /// <summary>
        /// Creates text files in the specified directory and fills them with random values in the given ranges
        /// </summary>
        /// <returns>String containing error message, null otherwise</returns>
        public static string GenerateDataFiles(string directoryPath, int numFiles, int numBins, int numItems, int minCapacity, int maxCapacity, int minLength, int maxLength)
        {
            //check that folder exists
            if ( !Directory.Exists(directoryPath))
                return "[ERROR_Testing] Directory doesn't exist at " + directoryPath;

            

            Random rnd = new Random();
            for(int i = 0; i < numFiles; i++)
            {
                List<int> binCapacities = new List<int>();
                //create bin file
                using (StreamWriter sw = new StreamWriter(directoryPath + @"\bin_" + i + ".txt"))
                {
                    for (int j = 0; j < numBins; j++)
                    {
                        int c = rnd.Next(minCapacity, ++maxCapacity);
                        binCapacities.Add(c);
                        sw.WriteLine("B" + j + " " + c + " " + ConvertBool(rnd.Next(0, 2)));
                    }
                }

                int maxC = binCapacities.OrderByDescending(c => c).First();
                maxC = maxC - Convert.ToInt32(Math.Round(maxC * 0.05));

                //create item file
                using (StreamWriter sw = new StreamWriter(directoryPath + @"\item_" + i + ".txt"))
                {
                    for (int j = 0; j < numItems; j++)
                    {
                        sw.WriteLine("I" + j + " " + rnd.Next(minLength, ++(maxC)));
                    }
                }


            }

            return null;
        }

        /// <summary>
        /// Solve all instances found in the specified directory;
        /// the instances are the filenames matching the specified patterns
        /// </summary>
        /// <param name="directoryPath"></param>
        /// <param name="binFilePattern">The pattern for the bins' filenames (ex "bin_*.txt")</param>
        /// <param name="itemFilePattern">The pattern for the items' filenames (ex "item_*.txt")</param>
        /// <returns></returns>
        public static string SolveInstances(string directoryPath, string binFilePattern, string itemFilePattern)
        {
            if (!Directory.Exists(directoryPath))
                Console.WriteLine("[ERROR_Testing] Directory doesn't exist at " + directoryPath);

            string[] binFiles = Directory.GetFiles(directoryPath, binFilePattern);
            string[] itemFiles = Directory.GetFiles(directoryPath, itemFilePattern);

            if( binFiles.Length > itemFiles.Length )
                return "[ERROR_Testing] There are less item instances than there are bin instances ";

            IDataReader dr = new SimpleReader();

            for (int i = 0; i < binFiles.Length; i++)
            {
                PackWithComparison(dr.ReadFromFile(binFiles[i], itemFiles[i]));
            }

            return null;

        }


        /// <summary>
        /// Execute the normal binpacking algorithm, then the algorithm that prioritizes
        /// the available bins, compare the two results and save in the output file
        /// (if it is specified in Settings) the best result (less bins used)
        /// </summary>
        /// <param name="pm">The problem model to solve</param>
        /// if the file does not exist, it is created</param>
        public static void PackWithComparison(IProblemModel pm)
        {
            ISolutionModel psm = new SolutionModel();
            ISolutionModel bsm = new SolutionModel();
            PrioritySolver.Pack(pm, psm, Settings.Solver);
            BaseSolver.Pack(pm, bsm, Settings.Solver);

            if (psm.Solution.Count <= bsm.Solution.Count)
            {
                PrintSolution(psm);
            }
            else
            {
                PrintSolution(bsm);
            }
            string endStr = "\n---------------------------------------------\n" +
                "\t BaseSolver numBins = " + bsm.Solution.Count + "\n" +
                "\t PrioritySolver numBins = " + psm.Solution.Count +
                "\n---------------------------------------------\n";
            PrintEnd(endStr);
        }



        /// <summary>
        /// Execute the normal binpacking algorithm on given the problem model
        /// </summary>
        /// <param name="pm">The problem model containing the bins and items 
        /// to be packed</param>
        public static void PackNormal(IProblemModel pm)
        {
            ISolutionModel bsm = new SolutionModel();
               BaseSolver.Pack(pm, bsm, Settings.Solver);

            PrintSolution(bsm);
            string endStr = "\n---------------------------------------------\n" +
                "\t BaseSolver numBins = " + bsm.Solution.Count +
                "\n---------------------------------------------\n";
            PrintEnd(endStr);
        }

        /// <summary>
        /// Execute the binpacking algorithm on given the problem model,
        /// favoring the use of bins marked as available.
        /// </summary>
        /// <param name="pm">The problem model containing the bins and items 
        /// to be packed</param>
        public static void PackPriority(IProblemModel pm)
        {
            ISolutionModel psm = new SolutionModel();
                PrioritySolver.Pack(pm, psm, Settings.Solver);
            
            PrintSolution(psm);
            string endStr = "\n---------------------------------------------\n" +
                "\t Solver Type : " + Settings.Solver +
                "\t PrioritySolver numBins = " + psm.Solution.Count +
                "\n---------------------------------------------\n";
            PrintEnd(endStr);
        }


        private static void PrintSolution(ISolutionModel s)
        {
            if (Settings.OutputPath == "")
            {
                s.PrintResults();
            }
            else
            {
                s.PrintResults(Settings.OutputPath);
            }
        }

        private static void PrintEnd(string str)
        {
            if (Settings.OutputPath == "")
            {
                Console.WriteLine(str);
            }
            else
            {
                using (StreamWriter sw = File.AppendText(Settings.OutputPath))
                {
                    sw.WriteLine(str);
                }
            }
        }

        private static string ConvertBool(int i)
        {
            return (i == 0) ? "false" : "true";
        }
    }
}
