﻿using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using Binpacking.LinearSolver;
using Binpacking.Model;
using Binpacking.DataReader;


namespace Binpacking
{
    class Program
    {

        static void Main(string[] args)
        {
            /* solver settings */
            /*
            BaseSolver.OutputEnabled = true;
            BaseSolver.MipGap = 0.05; // treshold to limit solving time
            BaseSolver.TimeLimitMs = 60*1000;
            PrioritySolver.OutputEnabled = true;
            PrioritySolver.MipGap = 0.05;
            PrioritySolver.TimeLimitMs = 60 * 1000;
            */
            
            /* testing settings */
            /*
            string sname = "";
            solvers.TryGetValue("SCIP", out sname);
            Testing.SolverName = sname;
            */

            /* handle command line arguments */
            if(!ArgsReader.HandleArgs(new List<string>(args)))
            {
                return;
            }

            IDataReader dr = new SimpleReader();
            IProblemModel pm = dr.ReadFromFile(Settings.BinPath, Settings.ItemPath);

            switch (Settings.Mode)
            {
                case "": Testing.PackWithComparison(pm); break;
                case "normal": Testing.PackNormal(pm); break;
                case "priority": Testing.PackPriority(pm); break;
                default: Console.WriteLine("[ERROR_Program] Mode parameter has abnormal value"); break;
            }



            //Testing.GenerateDataFiles(folder, 10, 100, 50, 180, 650, 40, 650);
            //Testing.PackingWithComparison(ReadDataFromFile(folder + "bin_0.txt", folder + "item_0.txt"), folder + "out_0.txt");

            //Testing.SolveInstances(folder, "bin_*.txt", "item_*.txt", folder + "out_test.txt");
        }


        // for quick testing
        private static IProblemModel CreateTestData()
        {
            Bin[] bins = new Bin[] { new Bin("B01", 200, true),
                                     new Bin("B02", 300, true),
                                     new Bin("B03", 400, true),
            };
            Item[] items = new Item[] { new Item("I01", 100, 45, 90),
                                        new Item("I02", 50, 45, 45),
                                        new Item("I03", 50, 90, 45),
                                        new Item("I04", 20),
                                        new Item("I05", 300),
                                        new Item("I06", 60),
                                        new Item("I07", 40),
                                        new Item("I08", 100)};

            IProblemModel pm = new ProblemModel();
            string e = pm.ParseData(bins, items);
            return pm;
        }

    }
}
